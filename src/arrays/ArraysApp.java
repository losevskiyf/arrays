package arrays;

import java.util.Arrays;
import java.util.Random;

public class ArraysApp {

    private static boolean isStrictlyIncreasing(int [] array){
        for (int i=1; i<array.length; i++)
            if (array[i]<=array[i-1]) return false;
        return true;
    }

    public static void main(String[] args) {

        Random random = new Random();
        int[] array = new int[8];

        for(int i=0; i<array.length; i++)
            array[i] = random.nextInt(10)+1;

        System.out.println(Arrays.toString(array));

        if(isStrictlyIncreasing(array))
            System.out.println("Последовательность является строго возрастающей последовательностью");
        else
            System.out.println("Последовательность не является строго возрастающей последовательностью");

        for (int i=0; i<array.length; i++)
            if(i%2==1) array[i] = 0;

        System.out.println(Arrays.toString(array));

    }
}